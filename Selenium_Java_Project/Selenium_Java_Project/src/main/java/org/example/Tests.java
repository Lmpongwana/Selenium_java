package org.example;


import io.github.bonigarcia.wdm.config.DriverManagerType;
import io.github.bonigarcia.wdm.managers.ChromeDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;
import java.util.List;

import org.testng.annotations.*;
import org.testng.asserts.Assertion;


public class Tests {
    static WebDriver chromedriver;
    static Assertion softAssert ;
    @BeforeTest
    public static void Init() {

        ChromeDriverManager.getInstance(DriverManagerType.CHROME).setup();
        chromedriver = new ChromeDriver();
        chromedriver.manage().window().maximize();
        chromedriver.get("https://rahulshettyacademy.com/AutomationPractice/");
        chromedriver.manage().timeouts().implicitlyWait(Duration.ofMillis(5000));
        softAssert = new Assertion();

    }
    @Test
    public static void RadioButtonsTest()
    {
        //-------------------------------------------Radio buttons-------------------------------------------
        ClickWebElement("//input[@value='radio1']");
        ValidateCheckedRadioButton("1");

        ClickWebElement("//input[@value='radio2']");
        ValidateCheckedRadioButton("2");
    }
    @Test
    public static void SuggestionsButtonsTest(){
        //-------------------------------------------Suggestions buttons-------------------------------------------
        WebElement textBox = getWebElement("//input[@placeholder='Type to Select Countries']");
        textBox.sendKeys("South");

        ClickWebElement("//div[text()='South Africa']");

        textBox = getWebElement("//input[@placeholder='Type to Select Countries']");
        textBox.clear();
        textBox.sendKeys("Republic");
        textBox.sendKeys(Keys.ARROW_DOWN);
        textBox.sendKeys(Keys.ENTER);
    }
    @Test
    public static void CheckBoxesTest()
    {
        //-------------------------------------------CheckBoxes------------------------------------

        List<WebElement> checkBoxes = getListOfWebElements("//div[@id='checkbox-example']//input[contains(@name,'checkBoxOption')]");
        for (WebElement checkBox:checkBoxes) {
            checkBox.click();
        }

        for (WebElement checkBox:checkBoxes) {
            softAssert.assertEquals(checkBox.isSelected(),true);
        }

        checkBoxes.get(0).click();
        for (int i= 1; i< checkBoxes.size();i++) {
            softAssert.assertEquals(checkBoxes.get(i).isSelected(),true);
        }
    }
    @Test
    public static void HideShowTest()
    {
        //------------------------------------------------Show Hide---------------------------------------------
        ClickWebElement("//input[@value='Hide']");
        WebElement hiddenElement = getWebElement("//input[@id='displayed-text']");
        softAssert.assertEquals(hiddenElement.isDisplayed(),false);
        //softAssert.assertEquals(hiddenElement.getAttribute("style"),"display: none;");

        ClickWebElement("//input[@value='Show']");
        hiddenElement = getWebElement("//input[@id='displayed-text']");
        softAssert.assertEquals(hiddenElement.isDisplayed(),true);

    }
    @Test
    public static void WebTableTest()
    {
        //---------------------------------------------------Web table -----------------------------------------------------
        WebElement webElement = getWebElement("//legend[text()='Web Table Fixed header']/..//table[@id='product']//tr[descendant::td[text()='Joe'] and descendant::td[text()='Postman'] and  descendant::td[text()='Chennai'] ]/td[last()]");
        softAssert.assertEquals(webElement.getText(),"46");

        List<WebElement> tableElements = getListOfWebElements("//legend[text()='Web Table Fixed header']/..//table[@id='product']//tr/td[last()]");

        int amountTotal=0;
        for (WebElement tableElement:tableElements) {
            amountTotal+= Integer.parseInt(tableElement.getText());
        }

        webElement = getWebElement("//div[contains(text(),'Total Amount Collected:')]");
        int retriedTotal= Integer.parseInt(webElement.getText().replace("Total Amount Collected:","").trim());

        softAssert.assertEquals(amountTotal, retriedTotal);



    }
    @Test
    public static void iframeTest()
    {
        //------------------------------------------------------ iframe ------------------------------------------------------------
        softAssert.assertEquals( true ,checkElementExists("//iframe"));

        WebElement iframe = getWebElement("//iframe[@id='courses-iframe']");
        chromedriver.switchTo().frame(iframe);
        ClickWebElement(" //a[text()='Login']");

    }

    @AfterTest
    public static void CloseSession()
    {
        chromedriver.quit();
    }
    public static WebElement getWebElement(String xpath)
    {
        return chromedriver.findElement(By.xpath(xpath));
    }

    public static List<WebElement> getListOfWebElements(String xpath)
    {
        return chromedriver.findElements(By.xpath(xpath));
    }
    public static void ValidateCheckedRadioButton(String RadioButtonNumber)
    {
        List<WebElement> radioButtons = chromedriver.findElements(By.xpath("//div[@id='radio-btn-example']//input[contains(@value,'radio')]"));
        for (WebElement radioButton:radioButtons) {

            if (radioButton.isSelected())
            {
                String selectedRadioButtonName = radioButton.getAttribute("value");
                softAssert.assertEquals(selectedRadioButtonName,"radio"+RadioButtonNumber);
            }

        }
    }

    public static void ClickWebElement(String xpath)
    {
        WebElement radioButton1 = chromedriver.findElement(By.xpath(xpath));
        radioButton1.click();
    }

    public static boolean checkElementExists(String xpath)
    {
        try
        {
            chromedriver.findElement(By.xpath(xpath));
        }
        catch (NoSuchElementException ex)
        {
            return false;
        }

        return true;
    }



}