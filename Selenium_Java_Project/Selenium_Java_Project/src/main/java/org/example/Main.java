package org.example;


import io.github.bonigarcia.wdm.config.DriverManagerType;
import io.github.bonigarcia.wdm.managers.ChromeDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;
import java.util.List;
import org.testng.asserts.Assertion;
import org.testng.asserts.SoftAssert;


public class Main {
   static WebDriver chromedriver;
    static Assertion softAssert ;

   public static void main(String[] args) {

        ChromeDriverManager.getInstance(DriverManagerType.CHROME).setup();
        chromedriver = new ChromeDriver();
        chromedriver.manage().window().maximize();
        chromedriver.get("https://rahulshettyacademy.com/AutomationPractice/");
        chromedriver.manage().timeouts().implicitlyWait(Duration.ofMillis(5000));
        softAssert = new SoftAssert();

        RadioButtonsTest();
        SuggestionsButtonsTest();
        CheckBoxesTest();
        HideShowTest();
        WebTableTest();
        iframeTest();

       CloseSession();
    }

    public static void RadioButtonsTest()
    {
        //-------------------------------------------Radio buttons-------------------------------------------
        ClickWebElement("//input[@value='radio1']");
        ValidateCheckedRadioButton("1");

        ClickWebElement("//input[@value='radio2']");
        ValidateCheckedRadioButton("2");
    }

    public static void SuggestionsButtonsTest(){
        //-------------------------------------------Suggestions buttons-------------------------------------------
        WebElement textBox = getWebElement("//input[@placeholder='Type to Select Countries']");
        textBox.sendKeys("South");

        ClickWebElement("//div[text()='South Africa']");

        textBox = getWebElement("//input[@placeholder='Type to Select Countries']");
        textBox.clear();
        textBox.sendKeys("Republic");
        textBox.sendKeys(Keys.ARROW_DOWN);
        textBox.sendKeys(Keys.ENTER);
    }

    public static void CheckBoxesTest()
    {
        //-------------------------------------------CheckBoxes------------------------------------

        List<WebElement> checkBoxes = getListOfWebElements("//div[@id='checkbox-example']//input[contains(@name,'checkBoxOption')]");
        for (WebElement checkBox:checkBoxes) {
            checkBox.click();
        }

        for (WebElement checkBox:checkBoxes) {
            softAssert.assertEquals(checkBox.isSelected(),false);
        }

        checkBoxes.get(0).click();
        for (int i= 1; i< checkBoxes.size();i++) {
            softAssert.assertEquals(checkBoxes.get(i).isSelected(),false);
        }
    }

    public static void HideShowTest()
    {
        //------------------------------------------------Show Hide---------------------------------------------
        ClickWebElement("//input[@value='Hide']");
        WebElement hiddenElement = getWebElement("//input[@placeholder='Type to Select Countries']");
        softAssert.assertEquals(hiddenElement.isDisplayed(),false);


        ClickWebElement("//input[@value='Show']");
        hiddenElement = getWebElement("//input[@placeholder='Type to Select Countries']");
        softAssert.assertEquals(hiddenElement.isDisplayed(),true);

    }

    public static void WebTableTest()
    {
        //---------------------------------------------------Web table -----------------------------------------------------
        WebElement webElement = getWebElement("//legend[text()='Web Table Fixed header']/..//table[@id='product']//tr[descendant::td[text()='Joe'] and descendant::td[text()='Postman'] and  descendant::td[text()='Chennai'] ]/td[last()]");
        softAssert.assertEquals(webElement.getText(),"46");

        List<WebElement> tableElements = getListOfWebElements("//legend[text()='Web Table Fixed header']/..//table[@id='product']//tr/td[last()]");

        int amountTotal=0;
        for (WebElement tableElement:tableElements) {
            amountTotal+= Integer.parseInt(tableElement.getText());
        }

        webElement = getWebElement("//div[contains(text(),'Total Amount Collected:')]");
        int retriedTotal= Integer.parseInt(webElement.getText().replace("Total Amount Collected:","").trim());

        softAssert.assertEquals(amountTotal, retriedTotal);



    }

    public static void iframeTest()
    {
        //------------------------------------------------------ iframe ------------------------------------------------------------
        softAssert.assertEquals( true ,checkElementExists("//iframe"));

        WebElement iframe = getWebElement("//iframe[@id='courses-iframe']");
        chromedriver.switchTo().frame(iframe);
        ClickWebElement(" //a[text()='Login']");

    }


    public static void CloseSession()
    {
        chromedriver.quit();
    }
    public static WebElement getWebElement(String xpath)
    {
        return chromedriver.findElement(By.xpath(xpath));
    }
    //
    public static List<WebElement> getListOfWebElements(String xpath)
    {
        return chromedriver.findElements(By.xpath(xpath));
    }
    public static void ValidateCheckedRadioButton(String RadioButtonNumber)
    {
        List<WebElement> radioButtons = chromedriver.findElements(By.xpath("//div[@id='radio-btn-example']//input[contains(@value,'radio')]"));
        for (WebElement radioButton:radioButtons) {

            if (radioButton.isSelected())
            {
                String selectedRadioButtonName = radioButton.getAttribute("value");
                softAssert.assertEquals(selectedRadioButtonName,"radio"+RadioButtonNumber);
            }

        }
    }

    public static void ClickWebElement(String xpath)
    {
        WebElement radioButton1 = chromedriver.findElement(By.xpath(xpath));
        radioButton1.click();
    }

    public static boolean checkElementExists(String xpath)
    {
        try
        {
           chromedriver.findElement(By.xpath(xpath));
        }
        catch (NoSuchElementException ex)
        {
            return false;
        }

        return true;
    }



}