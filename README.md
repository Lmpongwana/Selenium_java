# Selenium_java
This is a selenium java project that runs a chrome UI test.

# Prerequisites:
1.	Must have java 1.8 installed
2.	Must have maven 4.0.0 installed
3.	Must have Chrome installed
# How to run test:
Tip: There is a quick video under “Media” folder to quickly show how to run scenarios.

1.	Clone the project anywhere on your PC
2.	Then on your IDE that you use, open the project as a Maven project.
-	On IntelliJ after opening the project you can right click on the “POM.xml” file and select “Add as Maven project” then you should be able to generate all the needed dependencies for the project
3.	Navigate to folder “Selenium_Java_Project/Selenium_Java_Project/src/main/java/org/example/” and double click the Test_Suite class to see all the tests created.
4.	To run all of them at once, right click on the Test_Suite class and run it.
5.	Else to run the rest individually, click on the play looking icon next to each method that has a “@Test” tag on top of it. 
6.	When tests are running correctly, a chrome driver should open and actions get executed then the browser closes once the test finishes.
